import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NewTimetableComponent } from './new-timetable/new-timetable.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'app-new-timetable', component: NewTimetableComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewTimetableComponent
  ],
  imports: [
    BrowserModule,
    PanelModule,
    ButtonModule,
    CardModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {useHash: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
